arrayTextAdapt
==============

A plugin for LimeSurvey to use dropdown in replace of input in array text.

This plugin was tested only with label sets on 6.X.

## Installation

You need [toolsDomDocument](https://gitlab.com/SondagesPro/coreAndTools/toolsDomDocument).

### Via GIT

- Go to your LimeSurvey Directory (version up to 5.0 only)
- Clone plugins/arrayTextAdapt directory `git clone https://gitlab.com/SondagesPro/QuestionSettingsType/arrayTextAdapt.git`

### Via ZIP dowload
- Download <http://extensions.sondages.pro/IMG/auto/arrayTextAdapt.zip> or <http://dl.sondages.pro/arrayTextAdapt.tar>
- Extract : `unzip arrayTextAdapt.zip`
- Move the directory to  plugins/ directory inside LimeSUrvey

## Documentation

- After adding your Array texts question type : go to survey settings / Plugins
- All array text question type column (Y-axis) are shown and you can choose the setting to use
- Demo survey <https://demo.sondages.pro/194714?newtest=Y>


## Home page & Copyright
- HomePage <http://extensions.sondages.pro/arraytextadapt/>
- Copyright © 2016-2018 Denis Chenu <http://sondages.pro>
- Copyright © 2016 Comité Régional du Tourisme de Bretagne  <http://www.tourismebretagne.com/>

Distributed under [GNU GENERAL AFFERO PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/agpl.txt) licence
