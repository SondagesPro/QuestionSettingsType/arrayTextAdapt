<?xml version="1.0" encoding="UTF-8"?>
<document>
 <LimeSurveyDocType>Label set</LimeSurveyDocType>
 <DBVersion>184</DBVersion>
 <labelsets>
  <fields>
   <fieldname>lid</fieldname>
   <fieldname>label_name</fieldname>
   <fieldname>languages</fieldname>
  </fields>
  <rows>
   <row>
    <lid><![CDATA[17]]></lid>
    <label_name><![CDATA[Accord : 7 tranches]]></label_name>
    <languages><![CDATA[en fr nl]]></languages>
   </row>
  </rows>
 </labelsets>
 <labels>
  <fields>
   <fieldname>lid</fieldname>
   <fieldname>code</fieldname>
   <fieldname>title</fieldname>
   <fieldname>sortorder</fieldname>
   <fieldname>language</fieldname>
   <fieldname>assessment_value</fieldname>
  </fields>
  <rows>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[01]]></code>
    <title><![CDATA[Strongly disagree]]></title>
    <sortorder><![CDATA[0]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[01]]></code>
    <title><![CDATA[Pas du tout d'accord]]></title>
    <sortorder><![CDATA[0]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[01]]></code>
    <title><![CDATA[Erg mee oneens]]></title>
    <sortorder><![CDATA[0]]></sortorder>
    <language><![CDATA[nl]]></language>
    <assessment_value><![CDATA[1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[02]]></code>
    <title><![CDATA[Disagree]]></title>
    <sortorder><![CDATA[1]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[02]]></code>
    <title><![CDATA[Pas d'accord]]></title>
    <sortorder><![CDATA[1]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[02]]></code>
    <title><![CDATA[Mee oneens]]></title>
    <sortorder><![CDATA[1]]></sortorder>
    <language><![CDATA[nl]]></language>
    <assessment_value><![CDATA[2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[03]]></code>
    <title><![CDATA[Slightly disagree]]></title>
    <sortorder><![CDATA[2]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[3]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[03]]></code>
    <title><![CDATA[Plutôt pas d'accord]]></title>
    <sortorder><![CDATA[2]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[3]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[03]]></code>
    <title><![CDATA[Enigszins mee oneens]]></title>
    <sortorder><![CDATA[2]]></sortorder>
    <language><![CDATA[nl]]></language>
    <assessment_value><![CDATA[3]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[04]]></code>
    <title><![CDATA[Undecided]]></title>
    <sortorder><![CDATA[3]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[4]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[04]]></code>
    <title><![CDATA[Ni d'accord, ni pas d'accord]]></title>
    <sortorder><![CDATA[3]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[4]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[04]]></code>
    <title><![CDATA[Neutraal]]></title>
    <sortorder><![CDATA[3]]></sortorder>
    <language><![CDATA[nl]]></language>
    <assessment_value><![CDATA[4]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[05]]></code>
    <title><![CDATA[Slightly agree]]></title>
    <sortorder><![CDATA[4]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[5]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[05]]></code>
    <title><![CDATA[Plutôt daccord]]></title>
    <sortorder><![CDATA[4]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[5]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[05]]></code>
    <title><![CDATA[Enigszins mee eens]]></title>
    <sortorder><![CDATA[4]]></sortorder>
    <language><![CDATA[nl]]></language>
    <assessment_value><![CDATA[5]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[06]]></code>
    <title><![CDATA[Agree]]></title>
    <sortorder><![CDATA[5]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[6]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[06]]></code>
    <title><![CDATA[D'accord]]></title>
    <sortorder><![CDATA[5]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[6]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[06]]></code>
    <title><![CDATA[Mee eens]]></title>
    <sortorder><![CDATA[5]]></sortorder>
    <language><![CDATA[nl]]></language>
    <assessment_value><![CDATA[6]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[07]]></code>
    <title><![CDATA[Strongly agree]]></title>
    <sortorder><![CDATA[6]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[7]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[07]]></code>
    <title><![CDATA[Tout à fait d'accord]]></title>
    <sortorder><![CDATA[6]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[7]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[17]]></lid>
    <code><![CDATA[07]]></code>
    <title><![CDATA[Erg mee eens]]></title>
    <sortorder><![CDATA[6]]></sortorder>
    <language><![CDATA[nl]]></language>
    <assessment_value><![CDATA[7]]></assessment_value>
   </row>
  </rows>
 </labels>
</document>
