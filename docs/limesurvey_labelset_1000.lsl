<?xml version="1.0" encoding="UTF-8"?>
<document>
 <LimeSurveyDocType>Label set</LimeSurveyDocType>
 <DBVersion>356</DBVersion>
 <labelsets>
  <fields>
   <fieldname>lid</fieldname>
   <fieldname>label_name</fieldname>
   <fieldname>languages</fieldname>
  </fields>
  <rows>
   <row>
    <lid><![CDATA[1000]]></lid>
    <label_name><![CDATA[Niveau : 10 Point (De peu à beaucoup)]]></label_name>
    <languages><![CDATA[en fr]]></languages>
   </row>
  </rows>
 </labelsets>
 <labels>
  <fields>
   <fieldname>lid</fieldname>
   <fieldname>code</fieldname>
   <fieldname>title</fieldname>
   <fieldname>sortorder</fieldname>
   <fieldname>language</fieldname>
   <fieldname>assessment_value</fieldname>
  </fields>
  <rows>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[1]]></code>
    <title><![CDATA[not very<br />1]]></title>
    <sortorder><![CDATA[0]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[1]]></code>
    <title><![CDATA[très peu<br />1]]></title>
    <sortorder><![CDATA[0]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[2]]></code>
    <title><![CDATA[2]]></title>
    <sortorder><![CDATA[1]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[2]]></code>
    <title><![CDATA[2]]></title>
    <sortorder><![CDATA[1]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[3]]></code>
    <title><![CDATA[3]]></title>
    <sortorder><![CDATA[2]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[3]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[3]]></code>
    <title><![CDATA[3]]></title>
    <sortorder><![CDATA[2]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[3]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[4]]></code>
    <title><![CDATA[4]]></title>
    <sortorder><![CDATA[3]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[4]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[4]]></code>
    <title><![CDATA[4]]></title>
    <sortorder><![CDATA[3]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[4]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[5]]></code>
    <title><![CDATA[5]]></title>
    <sortorder><![CDATA[4]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[5]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[5]]></code>
    <title><![CDATA[5]]></title>
    <sortorder><![CDATA[4]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[5]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[6]]></code>
    <title><![CDATA[6]]></title>
    <sortorder><![CDATA[5]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[6]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[6]]></code>
    <title><![CDATA[6]]></title>
    <sortorder><![CDATA[5]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[6]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[7]]></code>
    <title><![CDATA[7]]></title>
    <sortorder><![CDATA[6]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[7]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[7]]></code>
    <title><![CDATA[7]]></title>
    <sortorder><![CDATA[6]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[7]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[8]]></code>
    <title><![CDATA[8]]></title>
    <sortorder><![CDATA[7]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[8]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[8]]></code>
    <title><![CDATA[8]]></title>
    <sortorder><![CDATA[7]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[8]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[9]]></code>
    <title><![CDATA[9]]></title>
    <sortorder><![CDATA[8]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[9]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[9]]></code>
    <title><![CDATA[9]]></title>
    <sortorder><![CDATA[8]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[9]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[10]]></code>
    <title><![CDATA[very<br />10]]></title>
    <sortorder><![CDATA[9]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[10]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[1000]]></lid>
    <code><![CDATA[10]]></code>
    <title><![CDATA[beaucoup<br />10]]></title>
    <sortorder><![CDATA[9]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[10]]></assessment_value>
   </row>
  </rows>
 </labels>
</document>
