<?xml version="1.0" encoding="UTF-8"?>
<document>
 <LimeSurveyDocType>Label set</LimeSurveyDocType>
 <DBVersion>184</DBVersion>
 <labelsets>
  <fields>
   <fieldname>lid</fieldname>
   <fieldname>label_name</fieldname>
   <fieldname>languages</fieldname>
  </fields>
  <rows>
   <row>
    <lid><![CDATA[16]]></lid>
    <label_name><![CDATA[Accord 5 tranches]]></label_name>
    <languages><![CDATA[en fr]]></languages>
   </row>
  </rows>
 </labelsets>
 <labels>
  <fields>
   <fieldname>lid</fieldname>
   <fieldname>code</fieldname>
   <fieldname>title</fieldname>
   <fieldname>sortorder</fieldname>
   <fieldname>language</fieldname>
   <fieldname>assessment_value</fieldname>
  </fields>
  <rows>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[01]]></code>
    <title><![CDATA[Agree strongly]]></title>
    <sortorder><![CDATA[0]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[-2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[01]]></code>
    <title><![CDATA[Complétement d'accord]]></title>
    <sortorder><![CDATA[0]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[-2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[02]]></code>
    <title><![CDATA[Agree somewhat]]></title>
    <sortorder><![CDATA[1]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[-1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[02]]></code>
    <title><![CDATA[Assez d'accord]]></title>
    <sortorder><![CDATA[1]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[-1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[03]]></code>
    <title><![CDATA[Neither agree nor disagree]]></title>
    <sortorder><![CDATA[2]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[0]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[03]]></code>
    <title><![CDATA[Ni d'accord, ni pas d'accord]]></title>
    <sortorder><![CDATA[2]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[0]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[04]]></code>
    <title><![CDATA[Disagree somewhat]]></title>
    <sortorder><![CDATA[3]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[04]]></code>
    <title><![CDATA[Plutôt en désaccord]]></title>
    <sortorder><![CDATA[3]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[1]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[05]]></code>
    <title><![CDATA[Disagree strongly]]></title>
    <sortorder><![CDATA[4]]></sortorder>
    <language><![CDATA[en]]></language>
    <assessment_value><![CDATA[2]]></assessment_value>
   </row>
   <row>
    <lid><![CDATA[16]]></lid>
    <code><![CDATA[05]]></code>
    <title><![CDATA[Complétement en désaccord]]></title>
    <sortorder><![CDATA[4]]></sortorder>
    <language><![CDATA[fr]]></language>
    <assessment_value><![CDATA[2]]></assessment_value>
   </row>
  </rows>
 </labels>
</document>
